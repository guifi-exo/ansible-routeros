# Ansible routeros
## Description
This repository stores some useful ansible commands and playbooks to massive apply changes to routers running RouterOS.

## Hosts
We included one hosts example that contains how routeros devices should be declared

## Playbooks
### Upgrade OS
In order to upgrade routeros version we have develop one playbook: `upgrade-routeros.yaml`

The next command shows an example how to run this playbook:

    ansible-playbook -i <hosts.file> upgrade-routeros.yaml
